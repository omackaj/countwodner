module Create exposing (..)

import Browser
import Html exposing (..)

main =
  Browser.sandbox { init = 0, update = update, view = view }

type Msg = Increment | Decrement

update msg model =
  case msg of
    Increment ->
      model + 1

    Decrement ->
      model - 1

view model =
  h1 [] [ text "Create" ]
    