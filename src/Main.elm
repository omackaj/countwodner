module Main exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes exposing (style)
import Html.Events exposing (onClick)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Task
import Time exposing (Posix)
import Debug
import TimeFormatter exposing (formatTime)
import Iso8601
import Url.Builder as Url
import Parser exposing (..)

main : Program () Model Msg
main =
    Browser.element
        { init = \_ -> init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }

    
-- MODEL

type alias Model =
    { left : Int
    , running : Bool
    , deadline : Posix
    }

init : ( Model, Cmd Msg )
init =
    ( Model 0 False (Time.millisToPosix 0), send (Fetch 3))

subscriptions : Model -> Sub Msg
subscriptions model =
    if model.running then
        Time.every 1000 Tick
    else
        Sub.none

-- UPDATE

send : msg -> Cmd msg
send msg =
  Task.succeed msg
  |> Task.perform identity

type Msg
    = Tick Posix
    | Start Posix
    | Fetch Int
    | Fetched (Result Http.Error String)
    | ToPosix (Result (List DeadEnd) Posix)

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Tick time ->
            ( { model | left = (Time.posixToMillis model.deadline) - (Time.posixToMillis time) }
            , Cmd.none 
            )

        Start deadline ->
            ( { model | running = True }, Cmd.none )

        Fetch id ->
            ( model, fetchPlanet id )
        
        Fetched result ->
            case result of
                Ok timeStamp ->
                    ( model, send (ToPosix (Iso8601.toTime timeStamp)))

                Err _ ->
                    ( model , Cmd.none)
        
        ToPosix result ->
            case result of
                Ok deadline ->
                    ( { model | deadline = deadline }, send (Start model.deadline))

                Err _ ->
                    ( model, Cmd.none)

-- HTTP

api : String
api =
    "https://countdownr.herokuapp.com"

planetUrl : String
planetUrl =
    Url.crossOrigin api ["graphql"] []

encodeBody : Int -> Encode.Value
encodeBody value = 
    Encode.object 
        [ ("query", Encode.string ("query { deadline(id:" ++ String.fromInt value ++ ") { date }}")) ]          

fetchPlanet value =
    Http.send Fetched (Http.post planetUrl (encodeBody value |> Http.jsonBody) responseDecoder)


responseDecoder : Decode.Decoder String
responseDecoder =
    Decode.field "data" (Decode.field "deadline" (Decode.field "date" Decode.string))

-- VIEW

view model =
    div
        [ style "background" "#010001"
        , style "padding-top" "6rem"
        , style "height" "100vh"
        , style "weight" "100vw"
        ]
        [ title
        , counter model
        , modelDebug modelDebug
        ]


wrap content =
    div
        [ style "background" "#010001"
        , style "padding-top" "6rem"
        , style "height" "100vh"
        , style "weight" "100vw"
        ]
        [ content ]
        
title =
    h1 
        [ style "color" "rgba(255, 255, 255, 0.8)" 
        , style "font-size" "2em"
        , style "text-align" "center"
        , style "padding-bottom" "5rem"
        , style "letter-spacing" "0.2rem"
        ] 
        [ text "Countdowner" ]

counter model =
    p 
        [ style "color" "rgba(255, 255, 255, 1)" 
        , style "font-size" "8em"
        , style "font-weight" "bold"
        , style "text-align" "center"
        , style "text-shadow" "rgb(93, 253, 223) 3px 3px 0px, rgb(252, 0, 102) -3px -3px 0px"
        ] 
        [text <| formatTime model.left]

modelDebug model =
    p 
        [ style "color" "rgba(255, 255, 255, 0.4)"
        , style "font-size" "0.7em"
        ]
        [text <| Debug.toString <| model]