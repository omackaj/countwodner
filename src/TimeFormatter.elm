module TimeFormatter exposing (formatTime)
import Time

secondConst = 1000
minuteConst = secondConst * 60
hourConst = minuteConst * 60

formatTime : Int -> String
formatTime millies =
    let
        hours = millies // hourConst
        minutes = remainderBy hourConst millies // minuteConst
        secondes = remainderBy minuteConst millies // secondConst
    in
        String.fromInt hours ++ "h " ++ getTimeString minutes ++ "m " ++ getTimeString secondes ++ "s"

getTimeString time = (String.padLeft 2 '0' <| String.fromInt time)
